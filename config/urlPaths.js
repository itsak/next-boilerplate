const servicePathMapUrls = {}
const productPathMapUrls = {}
const removePaths = ['/services/details', '/products/details']
/**
 * Service Page URLs
 */
const servicePathMap = [
    { url: 'annual-maintenance-contract-solutions' },
    { url: 'network-cabling-and-structured-cabling' },
    { url: 'data-center-infrastructure-solutions' },
    { url: 'security-and-surveillance-system' },
    { url: 'intruder-burglar-alarm-systems' },
    { url: 'data-center-monitoring-systems' },
    { url: 'cabinets-and-server-racks' },
    { url: 'telephone-epabx-systems' },
    { url: 'network-and-wi-fi' },
    { url: 'raised-flooring' },
]

servicePathMap.map(item => {
    return (servicePathMapUrls['/services/' + item.url] = {
        page: '/services/details',
        query: { url: item.url },
    })
})

const productsPathMap = [
    { url: 'alarm-systems' },
    { url: 'power-solutions' },
    { url: 'cctv' },
    { url: 'networking-and-wireless-solutions' },
    { url: 'telephone-epabx-systems' },
    { url: 'structured-cabling' },
    { url: 'datacenter-solutions' },
    { url: 'cabinets-and-server-racks' },
]

productsPathMap.map(item => {
    return (productPathMapUrls['/products/' + item.url] = {
        page: '/products/details',
        query: { url: item.url },
    })
})

module.exports = { removePaths, servicePathMapUrls, productPathMapUrls }
