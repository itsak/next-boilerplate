import React, { Component } from 'react'
import Head from 'next/head'
import Header from '../components/Header'
import Footer from '../components/Footer'
import '../assets/styles/style.scss'
export default class Layout extends Component {
    render() {
        const { children } = this.props
        return (
            <div className="body-wrapper">
                <Head>
                    <meta name="mobile-web-app-capable" content="yes" />
					<meta name="apple-mobile-web-app-capable" content="yes" />
					<meta
						name="viewport"
						content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"
					/>
					<meta name="apple-mobile-web-app-status-bar-style" content="black" />
					<meta name="theme-color" content="#000" />
					<meta name="format-detection" content="telephone=no" />
                </Head>
                <Header />
                <React.Fragment>{children}</React.Fragment>
                <Footer />
            </div>
        )
    }
}
