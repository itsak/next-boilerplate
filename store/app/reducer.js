import * as actionTypes from "../actionTypes";

const initialState = {
    isLoading: false,
    user:{
        isAuthenticated: false,
        data:{}
    },
    errors:{},
    messages:{},
    historyData:{},
    tableData:{
        data:[],
        links: {},
        meta: {
            page:{}
        }
    },
}


const app = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.APP_IS_LOADING:
            return {
                ...state,
                user:{
                    ...state.user,
                    newData: true
                },
                isLoading: true
            }
        case actionTypes.LOAD_DATA:
            return {
                ...state,
                tableData: action.payload
            }
        default:
            return state
    }
}

export default app
