import * as actionTypes from "../actionTypes";

export const isLoading = (payload) => {
    return {
        type: actionTypes.APP_IS_LOADING,
        payload
    }
}

export const loadData = (payload) => {
    return {
        type: actionTypes.LOAD_DATA,
        payload
    }
}