const dotEnvResult = require('dotenv').config()
const withSass = require('@zeit/next-sass')
const urlPaths = require('./config/urlPaths')
const { servicePathMapUrls, removePaths, productPathMapUrls } = urlPaths

const nextConfig = {
    sassLoaderOptions: {
        includePaths: ['node_modules/', 'assets/'],
    },
    publicRuntimeConfig: {
        staticFolder: process.env.STATIC_FOLDER_PATH || '/static',
    },
    assetPrefix: process.env.ASSET_PREFIX || '',
    exportPathMap: async function(defaultPathMap) {
        removePaths.map(item => {
            delete defaultPathMap[item]
        })
        const exportPaths = Object.assign(
            defaultPathMap,
            servicePathMapUrls,
            productPathMapUrls
        )
        return exportPaths
    },
}

module.exports = withSass(nextConfig)
