import Head from 'next/head'
export default ({ data = ''}) => {
    return (
        <Head>
            <title>{`${data.title} | Technocom Networking Communications | TNC`}</title>
            <meta name="description" content={`${data.description}`} />
            <meta name="keywords" content={`${data.keywords}`} />
            <meta property="og:title" content={`${data.title} | Technocom Networking Communications | TNC`} />
            <meta property="og:type" content="article" />
            <meta property="og:url" content="http://www.tncuae.com/" />
            <meta property="og:image" content="http://www.tncuae.com/static/assets/og-tnc.jpg" />
            <meta property="og:description" content={`${data.description}`} />
            <link href="/static/favicon.ico" rel="shortcut icon" />
            <link rel="manifest" href="/static/manifest.json"/>
            <meta name="mobile-web-app-capable" content="yes"/>
            <meta name="apple-mobile-web-app-capable" content="yes"/>
            <meta name="application-name" content="TNC"/>
            <meta name="apple-mobile-web-app-title" content="TNC"/>
            <meta name="theme-color" content="#0F4D90"/>
            <meta name="msapplication-navbutton-color" content="#0F4D90"/>
            <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
            <meta name="msapplication-starturl" content="/"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            <link rel="icon" href="/static/assets/images/ic_mainlogo@2x.png"/>
            <link rel="apple-touch-icon" href="/static/assets/images/ic_mainlogo@2x.png"/>
        </Head>
    )
}