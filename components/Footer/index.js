import ScrollAnimation from 'react-animate-on-scroll'
import ActiveLink from '../ActiveLink'
import './index.scss'
// import PrivacyPolicy from './PrivacyPolicy'

export default () => {
    return (
        <div className="footer-wrapper">
            <footer className="footer clearfix">
                <ScrollAnimation
                    offset={0}
                    animateIn="fadeInUp"
                    animateOnce={true}
                >
                    <div className="container">
                        <div className="nav-holder">
                            <ul>
                                <li>
                                    <ActiveLink
                                        exact={true}
                                        activeClassName="active"
                                        href="/about-us"
                                    >
                                        About us
                                    </ActiveLink>
                                </li>
                                <li>
                                    <ActiveLink
                                        exact={true}
                                        activeClassName="active"
                                        href="/terms-and-conditions"
                                    >
                                        Terms and conditions
                                    </ActiveLink>
                                </li>
                                <li>
                                    <ActiveLink
                                        exact={true}
                                        activeClassName="active"
                                        href="/quality-policy"
                                    >
                                        Quality policy
                                    </ActiveLink>
                                </li>
                                <li>
                                    <ActiveLink
                                        exact={true}
                                        activeClassName="active"
                                        href="/privacy-policy"
                                    >
                                        Privacy policy
                                    </ActiveLink>
                                </li>
                                <li>
                                    <ActiveLink
                                        exact={true}
                                        activeClassName="active"
                                        href="#"
                                    >
                                        <img
                                            src="/static/assets/images/linkedin.png"
                                            alt="ActiveLinkedin"
                                        />
                                    </ActiveLink>
                                </li>
                            </ul>
                        </div>
                        <div className="copyright">
                            <p>
                                &copy; 2018 - Technocom Networking Communication
                                LLC
                            </p>
                        </div>
                    </div>
                </ScrollAnimation>
            </footer>
            {/* <PrivacyPolicy/> */}
        </div>
    )
}
