import { withRouter } from 'next/router'

const ActiveLink = ({
    children,
    router,
    href,
    activeClassName = '',
    className = '',
}) => {
    const clsName =
        router.asPath === href ? `${className} ${activeClassName}` : className
    const handleClick = e => {
        e.preventDefault()
        router.push(href)
    }

    return (
        <a href={href} onClick={handleClick} className={clsName}>
            {children}
        </a>
    )
}

export default withRouter(ActiveLink)
