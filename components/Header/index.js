import ActiveLink from '../ActiveLink';
import './index.scss';
const Header = ({ v = 1 }) => {
	return (
		<header className={`header clearfix ${v === 2 ? 'header-2' : ''}`}>
			<div className="container clearfix clear">
				<div className="logo-holder">
					<ActiveLink href="/">
						<img
							src={`/static/assets/images/${v === 2
								? 'logo_main_white@2x.png'
								: 'logo_main@2x.png'}`}
							alt="Technocom Networking"
						/>
					</ActiveLink>
				</div>
				<div className="nav-holder">
					<ul>
						<li>
							<ActiveLink activeClassName="active" href="/">
								Home
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/about-us">
								About Us
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/services">
								Services
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/products">
								Products
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/clients">
								Clients
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/careers">
								Careers
							</ActiveLink>
						</li>
						<li>
							<ActiveLink activeClassName="active" href="/contact">
								Contact
							</ActiveLink>
						</li>
					</ul>
				</div>
			</div>
		</header>
	);
};

export default Header;
