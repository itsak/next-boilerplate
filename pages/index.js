import MetaTags from '../components/MetaTags'
import Layout from '../layout/'
import meta from './home/meta.json'
export default () => (
    <Layout>
        <MetaTags data={meta} />
    </Layout>
)