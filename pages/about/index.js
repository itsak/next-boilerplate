import ScrollAnimation from 'react-animate-on-scroll'
import MetaTags from '../../components/MetaTags'
import Layout from '../../layout'
import meta from './meta.json'
export default () => (
    <Layout>
        <MetaTags data={meta} />
        <div className="section">
            <div className="container about-us">
                <div className="text-center">
                    <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                        <img
                            src="/static/assets/images/img_main_hompage.svg"
                            alt="About Us"
                            className="header-icon-image"
                        />
                    </ScrollAnimation>
                </div>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <h1 className="headline">About us</h1>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <p className="description">
                        Technocom Networking Communication LLC is an ISO-9001
                        certified company ,expertise in all IT and networking
                        solutions. Technocom is one of the fast growing company
                        who deals with all type of business solution.
                    </p>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <p className="description">
                        We were established in 2005 as a networking company and
                        expanded our services to provide full IT solutions all
                        over UAE. Technocom provides the complete
                        business solution for large, medium and small customer
                        solution which includes network solution, IP telephony,
                        telecommunication, data centre infrastructure, planning,
                        customer care centre planning, business application, Elv
                        system, professional audio visual and broadcasting
                        solutions.
                    </p>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <p className="description">
                        The continual market study, research and wealth of ideas
                        are the center of our company activities as it helped us
                        to taste the sweetness of customer satisfaction. The
                        success of every business is customer satisfaction.
                    </p>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <h2 className="headline">Mission</h2>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <p className="description">
                        Our mission is to provide expertise services coupled
                        with quality.
                    </p>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <h2 className="headline">Vision</h2>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeInUp" animateOnce={true}>
                    <p className="description">
                        Our aim is to be the largest and the most reliable
                        networking and security system company in the United
                        Arab Emirates and gain enough resources to build it to a
                        new dimension and also diverse our activities as much as
                        possible to be provide complete quality products and
                        services. We also aim to achieve a dynamic management
                        team that can serve our clients quickly and efficiently
                        and also be able to conquer the networking market of the
                        UAE.
                    </p>
                </ScrollAnimation>
            </div>
        </div>
    </Layout>
)
